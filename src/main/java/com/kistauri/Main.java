package com.kistauri;

public class Main {

    public static void main(String[] args) {
        int a = Integer.parseInt(System.getenv("VAR_1"));
        int n = Integer.parseInt(System.getenv("VAR_2"));

        if (a == 0 && n <= 0) {
            throw new ArithmeticException("Невозможно возвести 0 в степень меньше 1");
        }
        if (a == 1 || n == 0)
            System.out.println("1");

        System.out.println(a + " в степени " + n+" = "+pow(a, n));
    }

    public static int pow(int value, int powValue) {
        if (powValue == 1) {
            return value;
        } else {
            return value * pow(value, powValue - 1);
        }
    }
}
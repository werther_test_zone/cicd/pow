import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

import com.kistauri.Main;

public class TestPow {
    @Test
    public void testApowN() {
        assertEquals(25, Main.pow(5, 2));
    }
}

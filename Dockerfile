FROM maven:3.9.6-eclipse-temurin-17-alpine as build

ARG PROJECT_NAME=werther

RUN apk add --no-cache binutils

COPY . .
RUN mvn package -DskipTests

RUN jdeps --ignore-missing-deps -q  \
    --recursive  \
    --multi-release 17  \
    --print-module-deps  \
    target/${PROJECT_NAME}.jar > deps.info

RUN jlink \
    --add-modules $(cat deps.info) \
    --strip-debug \
    --no-man-pages \
    --no-header-files \
    --compress 2 \
    --output /myjre

FROM alpine:3.19.0

ENV JAVA_HOME /jre
ENV PATH $JAVA_HOME/bin:$PATH

COPY --from=build /myjre $JAVA_HOME

ARG USER=user
RUN adduser --no-create-home -u 1000 -D $USER

RUN mkdir /app && chown -R $USER /app

USER 1000

COPY --from=build target/*.jar /app/app.jar

WORKDIR /app

ENTRYPOINT java -jar app.jar